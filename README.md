## Project Name 
- merchanthub

## Project Links
- [Project Link](https://merchanthub.onrender.com/)
- [Test Suite](https://cloudy-resonance-875592.postman.co/workspace/My-Workspace~23758ce0-3448-464d-8f40-2b3a5118f28a/overview)
- [UI Design](https://app.uizard.io/p/ab085b91/overview)
- [Database Designs](https://drive.google.com/file/d/1YYEefpYC3Fo8qpEElyySoGk-Igh0VLzi/view?usp=sharing)

## Functional Specifications 
***Business Account***
1. Account Creation:
- Businesses can register accounts with essential information (name, contact details, location, description) on the MerchantHub app.
2. Profile Management:
- Upload logos, photos, and videos to showcase offerings.
- Manage menus, service listings, or appointment slots.
- Set pricing and availability.

***Customer Functionality*** 
1. Browsing:
- Browse businesses by category, location, or keywords on the MerchantHub application.
- View detailed business profiles, menus, service descriptions, or appointment schedules.
2. Ordering and Booking:
- Place orders for online delivery or pickup.
- Book appointments for services.
3. Interactions:
- Leave reviews and ratings for businesses.

## Technical Specifications:

***Frontend***
- HTML
- CSS
- Javascript

***Backend:***
- Framework: Django for server-side development.
- Database: SQLite for storing users, businesses.

***Third-Party Integrations:***
- Email API: MerchantHub utilises  SendGrid  for order confirmations. 

***Cloud Hosting:***
- Merchant Hub will utilize AWS for web application hosting.


## Members of this Project 
- 👤 [NTONDE EDGAR ISINGOMA - 23/U/24810/EVE](https://gitlab.com/eddiisingoma)
- 👤 [NAGGAYI DAPHNE PEARL - 23/U/13097/EVE](https://gitlab.com/daphnepearl101)
- 👤 [NALUNKUMA KETRA - 23/U/14140/EVE](https://gitlab.com/Caterfall)
- 👤 [ASIINGWIRE ERIC - 23/U/06707/EVE](https://gitlab.com/latimore14)
- 👤 [NABBAALE CLAIRE - 23/U/12770/EVE](https://gitlab.com/claireleah256)
