## Project Name

- Merchant Hub - Front end Repository

## Live Demo

- [front end link](https://merchanthub-frontend-orcin.vercel.app/)
- [backend link](https://merchanthub.onrender.com/)
- [Test Suite](https://app.getpostman.com/join-team?invite_code=3dc80bb8724e58edf44790082732ca57&target_code=a6a977331cabdcaca0533e1b37751f0d)

## Technical Specifications:

**_Frontend_**

- HTML
- CSS
- Javascript
- Bootstrap

**_Backend:_**

- Framework: Django for server-side development.
- Database: SQLite for storing users, businesses.

**_Third-Party Integrations:_**

- Email API: MerchantHub utilises SendGrid for order confirmations.


## Members of this Project

- 👤 [NTONDE EDGAR ISINGOMA - 23/U/24810/EVE](https://gitlab.com/eddiisingoma)
- 👤 [NAGGAYI DAPHNE PEARL - 23/U/13097/EVE](https://gitlab.com/daphnepearl101)
- 👤 [NALUNKUMA KETRA - 23/U/14140/EVE](https://gitlab.com/Caterfall)
- 👤 [ASIINGWIRE ERIC - 23/U/06707/EVE](https://gitlab.com/latimore14)
- 👤 [NABBAALE CLAIRE - 23/U/12770/EVE](https://gitlab.com/claireleah256)
