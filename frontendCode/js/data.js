const form = document.getElementById("loginForm");

form.addEventListener("submit", async (e) => {
  e.preventDefault();

  const formData = new FormData(form);

  try {
    const response = await fetch(
      "https://merchanthub.onrender.com/loginuser/",
      {
        method: "POST",
        body: formData,
      }
    );

    if (response.ok) {
      // Request successful
      console.log("Login successful");
    } else {
      // Request failed
      console.error("Login failed");
    }
  } catch (error) {
    console.error("Error:", error);
  }
});
