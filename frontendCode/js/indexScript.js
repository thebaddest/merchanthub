populateCat('Electronics')
populateCat('Stationery')
populateCat('Clothing')
populateCat('Toys')

function populateCat(catName) {
  fetch(`https://merchanthub.onrender.com/itemsbycat?category=${catName}`)
    // fetch(`http://127.0.0.1:8000/itemsbycat?category=${catName}`)
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then((products) => {
      products = Object.values(products);
      const productCarouselInnerWomen = document.querySelector(
        `#productCarousel${catName} .carousel-inner`
      );
      let index = 0;
      // Iterate over products in groups of 4
      while (index < products.length) {
        const carouselItem = document.createElement("div");
        carouselItem.classList.add("carousel-item");
        if (index === 0) {
          carouselItem.classList.add("active");
        }
        const row = document.createElement("div");
        row.classList.add("row");

        for (let i = 0; i < 4 && index < products.length; i++, index++) {
          const product = products[index];
          const col = document.createElement("div");
          col.classList.add("col-md-3");
          col.innerHTML = `
                    <div class="card">
                        <h5 class="name">${product.name}</h5>
                        <img class="card-img-top images" src="./hundredImages/${
                          product.name
                        }.jpeg"></img>
                        <p class="price">UGX: ${product.price}</p>
                        <p class="rateNstock">Rating: ${getRate(
                          product.rate1,
                          product.rate2,
                          product.rate3,
                          product.rate4,
                          product.rate5
                        )}  Stock: ${product.stock}</p>
                        <p class="business-info">Business: ${
                          product.businessInfo.businessName
                        }</p>
                        <a href="./product.details.html?id=${
                          product.itemID
                        }" class="button">Add to Cart</a>

                        </div>`;
          row.appendChild(col);
        }
        carouselItem.appendChild(row);

        productCarouselInnerWomen.appendChild(carouselItem);
      }
    })
    .catch((error) => {
      console.error("There was a problem with the fetch operation:", error);
    });
}
function getRate(rate1, rate2, rate3, rate4, rate5) {
  const ans =
    (+rate1 * 1 + +rate2 * 2 + +rate3 * 3 + +rate4 * 4 + +rate5 * 5) /
    (+rate1 + +rate2 + +rate3 + +rate4 + +rate5);
  return ans.toFixed(2);
}

//SEARCH FUNCTION
function searchResults() {
  const input = document.querySelector(".form-control");
  let inputVal = input.value;
  input.innerHTML = "";
  inputVal = inputVal.split(" ");
  fetch("https://merchanthub.onrender.com/searchitems/", {
    method: "POST",
    body: JSON.stringify({
      query: inputVal,
    }),
  })
    .then((resp) => resp.json())
    .then((data) => {
      const navbar = document.querySelector(".navbar");
      const items = document.createElement("div");
      items.className = "searchedItems";
      const footerAbout = document.querySelector("#aboutUS-container");
      const footerContainer = document.querySelector("#footer-container");
      const body = document.querySelector("body");
      while (body.firstChild) {
        body.firstChild.remove();
      }
      if (Object.values(data).length == 0) {
        console.log("No search results");
        //we are to create div class that says so
        return;
      }
      for (product in data) {
        if(data[product]=="Invalid data provided."){
            console.log(data.err)
            return
        }
        const prodDiv = `
                <div class="card searchCard">
                    <h5 class="name">${data[product].name}</h5>
                    <img class="card-img-top images" src="./hundredImages/${
                      data[product].name
                    }.jpeg"></img>
                    <p class="price">UGX: ${data[product].price}</p>
                    <p class="rateNstock">Rating: ${getRate(
                      data[product].rate1,
                      data[product].rate2,
                      data[product].rate3,
                      data[product].rate4,
                      data[product].rate5
                    )}  Stock: ${data[product].stock}</p>
                    <p class="business-info">Business: ${
                      data[product].businessInfo.businessName
                    }</p>
                    <a href="./product.details.html?id=${
                        product.itemID
                      }" class="button">Add to Cart</a>
              </div>`;
        items.insertAdjacentHTML("beforeend", prodDiv);
      }
      body.appendChild(navbar);
      body.appendChild(items);
      body.appendChild(footerAbout);
      body.appendChild(footerContainer);
    });
}
