from django.contrib import admin
from .models  import *

class UserInfoAdmin(admin.ModelAdmin):
    list_display = ('userID', 'firstname', 'lastname', 'email', 'password', 'latitude', 'longitude', 'phone', 'accountActive', 'verificationCode')

class BusinessInfoAdmin(admin.ModelAdmin):
    list_display = ('businessName', 'businessID', 'userID', 'businessPhone', 'businessEmail', 'verificationCode', 'latitude', 'longitude',  'accountActive','supportsCash', 'supportsMobileMoney', 'supportsPaymentCard')

class EmailUpdateAdmin(admin.ModelAdmin):
    list_display = ('updateID', 'userID', 'businessID', 'email', 'verificationCode')

class NameAndCategoryAdmin(admin.ModelAdmin):
    list_display = ('category', 'name')

class ItemAdmin(admin.ModelAdmin):
    list_display = ('itemID', 'name', 'businessID', 'scheduleAppt', 'stock', 'price', 'latitude', 'longitude', 'description', 'rate1', 'rate2', 'rate3', 'rate4', 'rate5', 'itemDeleted')

class DiscountAdmin(admin.ModelAdmin):
    list_display = ('itemID', 'discountID', 'minQty', 'cashDiscounted', 'bonusItemID', 'offerExpiry')

class CardAdmin(admin.ModelAdmin):
    list_display = ('name', 'cardNum', 'userID', 'businessID', 'expDate', 'securityCode')

class ColoursNPicsAdmin(admin.ModelAdmin):
    list_display = ('coloursNPicsID', 'itemID', 'colour', 'photo')

class CustomerOrderAdmin(admin.ModelAdmin):
    list_display = ('orderID', 'userID', 'totalBill', 'paymentStatus', 'deliveryStatus', 'cancellationStatus', 'transactionDate')

class OrderDetailAdmin(admin.ModelAdmin):
    list_display = ('detailID','orderID', 'businessID', 'itemName','coloursNPicsID', 'quantity', 'latitude', 'longitude', 'serviceStart', 'serviceEnd')


admin.site.register(UserInfo, UserInfoAdmin)
admin.site.register(Card, CardAdmin)
admin.site.register(NameAndCategory, NameAndCategoryAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Discount, DiscountAdmin)
admin.site.register(BusinessInfo, BusinessInfoAdmin)
admin.site.register(EmailUpdate, EmailUpdateAdmin)
admin.site.register(ColoursNPics, ColoursNPicsAdmin)
admin.site.register(CustomerOrder, CustomerOrderAdmin)
admin.site.register(OrderDetail, OrderDetailAdmin)
