from django.http import JsonResponse
from . import models
from django.forms.models import model_to_dict
from django.db.models import Q
import json
import re
from django.utils import timezone
from random import random
from django.core.mail import send_mail
from django.conf import settings

def initialise(request):
    return JsonResponse({"message": "Check one two"})

def login_user(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            password = data['password']
            email = data['email']
            user_dict = models.UserInfo.objects.filter(email=email).first()
            if not user_dict:
                raise Exception("User not found")
            if not user_dict.accountActive:
                raise Exception("User account was deleted")
            if user_dict.verificationCode:
                raise Exception("Account not verified")           
            elif user_dict.password == password:
                user_dict = model_to_dict(user_dict)
                user_dict.pop('password')
                del user_dict['accountActive']
                del user_dict['verificationCode']           
                return JsonResponse({"message": 'Login Success', 'userDict': user_dict})
            else:
                return JsonResponse({'message': 'Password mismatch'})
        except BaseException as e:
            return JsonResponse({'message': 'Invalid data provided.', "err": e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def send_verif_email(email, code):
    subject = 'Verification code for MerchantHub'
    message = f'Thank you for registering for an account with MerchantHub. Your verification code is: {code}'
    from_email = settings.EMAIL_HOST_USER
    recipient_list = [email]
    return send_mail(subject, message, from_email, recipient_list, fail_silently=False)

def get_cards(request):
    if request.method== 'GET':
        try:
            userID = request.GET.get('userID')
            businessID = request.GET.get('businessID')
            cards = None
            if userID:
                cards = models.Card.objects.filter(userID=userID)
            elif businessID:
                cards = models.Card.objects.filter(businessID=businessID)
            if not cards:
                return JsonResponse({'message': 'No cards for provided ID'})
            cards_dict = {}
            for item in cards:
                card_dict = model_to_dict(item)
                cards_dict[card_dict['cardNum']]= card_dict
            return JsonResponse(cards_dict)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', "err": e.__str__()})
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def update_userinfo(request):
    if request.method == 'POST':
        try:
            updates = json.loads(request.body)
            user_record = models.UserInfo.objects.filter(userID=updates["userID"]).filter(verificationCode__isnull=True).first()
            del updates["userID"]
            if "email" in updates and not re.fullmatch(r"^[^\s@]+@[^\s@]+\.[^\s@]+$", updates['email']):
                raise Exception("Not valid email")
            if not user_record.accountActive:
                raise Exception("User account was deleted")
            for key in updates:
                if key in ["firstname", "lastname", "accountActive", "latitude", "longitude", "phone"]:
                    setattr(user_record, key, updates[key])
                elif key == "passwords":
                    if user_record.password == updates["passwords"][0]:
                        setattr(user_record, "password", updates["passwords"][1])
                    else:
                        raise Exception("Old password does not match")
                else:
                    verif_code = str(int(random()*1000000))
                    temp_user = models.EmailUpdate.objects.create(verificationCode=verif_code, userID=user_record)
                    temp_user.email = updates["email"]
                    mailSent = send_verif_email(updates['email'], verif_code)
                    if not mailSent:
                        raise Exception('Mail not sent')
                    temp_user.save()
            user_record.save()
            user_record = model_to_dict(user_record)
            del user_record['password']
            del user_record['verificationCode']
            del user_record['accountActive']
            del user_record['email'] #still reflecting the old one sometimes
            return JsonResponse(user_record)
        except Exception as e:
            temp_user = models.EmailUpdate.objects.filter(userID=user_record.userID).first()
            if temp_user:
                temp_user.delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def create_card(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            card_in_table = models.Card.objects.filter(cardNum=data['cardNum'])
            if card_in_table:
                return JsonResponse({"message": "Card already belongs to another account"})
            elif 'userID' in data:
                user_instance = models.UserInfo.objects.filter(userID=data['userID']).filter(verificationCode__isnull=True).first()
                models.Card.objects.create(userID=user_instance, name=data['name'],cardNum=data['cardNum'],expDate=data['expDate'], securityCode=data["securityCode"])
                return JsonResponse({"message": "Card created"})
            else:
                business_instance = models.BusinessInfo.objects.filter(businessID=data['businessID'], verificationCode__isnull=True).first()
                models.Card.objects.create(businessID=business_instance, name=data['name'],cardNum=data['cardNum'],expDate=data['expDate'], securityCode=data["securityCode"])
                return JsonResponse({"message": "Card created"})
        except BaseException as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def remove_card(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            realCard = models.Card.objects.filter(cardNum=data['cardNum']).first()#returns the first model of the queryset
            if not realCard:
                return JsonResponse({"message": "Card not found"})
            else:
                realCard.delete()
                return JsonResponse({"message":"Card deleted"})
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only DELETE requests are allowed.'})

def get_catalogue(request):
    if request.method == 'GET':
        try:
            records = models.NameAndCategory.objects.all()
            catalog = {
                'images': {}
            }
            for item in records:
                if item.category in catalog:
                    catalog[item.category].append(item.name)
                else:
                    catalog[item.category] = [item.name]
                    itemInstance = models.Item.objects.filter(name=item).order_by('?').first()
                    photoInstance = models.ColoursNPics.objects.filter(itemID=itemInstance).order_by('?').first()
                    catalog['images'][item.category] = photoInstance.photo

            return JsonResponse(catalog)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def items_by_name(request):
    if request.method == 'POST':
        try:
            id_body = json.loads(request.body)
            items_dict = item_data(id_body["names"], 'name')
            return JsonResponse(items_dict) 
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def items_by_itemID(request):
    if request.method == 'POST':
        try:
            id_body = json.loads(request.body)
            items_dict = item_data(id_body["ids"], 'id')
            return JsonResponse(items_dict) 
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def items_by_businessID(request):
    if request.method == 'GET':
        try:
            businessID = request.GET.get('businessID')
            ids = models.Item.objects.filter(businessID=businessID).values('itemID')
            ids = list(ids)
            for el in range(len(ids)):
                ids[el] = ids[el]['itemID']
            items_dict = item_data(ids, 'id')           
            return JsonResponse(items_dict)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def items_by_cat(request):
    if request.method == 'GET':
        try:
            category = request.GET.get("category")
            names = models.NameAndCategory.objects.filter(category=category).values('name')
            names = list(names)
            for el in range(len(names)):
                names[el] = names[el]['name']
            items_dict = item_data(names, 'name')           
            return JsonResponse(items_dict)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def item_data(namesOrIDs, colname):
    items_query = []
    if colname == 'name':
        for onename in namesOrIDs:
            oneDict = models.Item.objects.filter(name=onename, itemDeleted=False).order_by('?').values('name', 'itemID', 'businessID', 'scheduleAppt', 'stock', 'price', 'latitude', 'longitude', 'description', 'rate1', 'rate2', 'rate3', 'rate4', 'rate5').first()
            if oneDict:
                items_query.append(oneDict)
    elif colname == 'id':
        items_query = models.Item.objects.filter(itemID__in=namesOrIDs, itemDeleted=False).values('name', 'itemID', 'businessID', 'scheduleAppt', 'stock', 'price', 'latitude', 'longitude', 'description', 'rate1', 'rate2', 'rate3', 'rate4', 'rate5')
    else:
        return None
    big_dict = {}
    for item in items_query:
        big_dict[item['itemID']] = item
        businessInfo = models.BusinessInfo.objects.filter(businessID=item['businessID']).first()
        big_dict[item['itemID']]['businessInfo'] = model_to_dict(businessInfo)
        del big_dict[item['itemID']]['businessID']
        discounts = models.Discount.objects.filter(itemID=item['itemID']).filter(Q(offerExpiry__gte=timezone.now()) | Q(offerExpiry__isnull=True))
        big_dict[item['itemID']]['discounts'] = {}
        for disc in discounts:
            disc = model_to_dict(disc)
            del disc['itemID']
            big_dict[item['itemID']]['discounts'][disc['discountID']] = disc
        coloursNPics = models.ColoursNPics.objects.filter(itemID=item['itemID'])
        big_dict[item['itemID']]['coloursNPics'] = {}
        for colorpic in coloursNPics:
            colorpic = model_to_dict(colorpic)
            del colorpic['itemID']
            big_dict[item['itemID']]['coloursNPics'][colorpic['coloursNPicsID']] = colorpic
    
    return big_dict

def add_item(request):
    if request.method == 'POST':
        try:
            item = None #for exception events
            recordCol = {}
            recordDisc = {}
            data = json.loads(request.body)
            business = models.BusinessInfo.objects.filter(businessID=data['businessID'], accountActive=True).first()
            name = models.NameAndCategory.objects.filter(name=data['name']).first()
            item = models.Item.objects.create(businessID=business, name=name)
            del data['businessID']
            del data['name']
            coloursNPics = data.pop('coloursNPics', None)
            discounts = data.pop('discounts', None)
            for field in data:
                setattr(item, field, data[field])
            item.save()
            if coloursNPics:
                add_colsNpics(coloursNPics, recordCol, item)
            if discounts:
                add_discounts(discounts, recordDisc, item)
            return JsonResponse({"message": "Item created", "itemID": item.itemID})
        except Exception as e:
            if item:
                item.delete()
            if recordCol:
                for rec in recordCol:
                    recordCol[rec].delete()
            if recordDisc:
                for rec in recordDisc:
                    recordDisc[rec].delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def add_colsNpics(coloursNPics, recordCol, item):
    for obj in range(len(coloursNPics)):
        entry = 'rec'+ str(obj)
        recordCol[entry] = models.ColoursNPics.objects.create(itemID=item)
        for field in coloursNPics[obj]:
            setattr(recordCol[entry], field, coloursNPics[obj][field])
        recordCol[entry].save()

def add_discounts(discounts, recordDisc, item):
    for obj in range(len(discounts)):
        entry = 'rec' + str(obj)
        recordDisc[entry] = models.Discount.objects.create(itemID=item)
        bonusID = discounts[obj].pop('bonusItemID', None)
        for field in discounts[obj]:
            setattr(recordDisc[entry], field, discounts[obj][field])
        if bonusID:
            extra = models.Item.objects.filter(itemID=bonusID).first()
            setattr(recordDisc[entry], 'bonusItemID', extra)
        recordDisc[entry].save()

def update_item(request):
    if request.method == 'POST':
        try:
            recordCol = {}
            recordDisc = {}
            data = json.loads(request.body)
            item = models.Item.objects.filter(itemID=data['itemID']).first()
            rate = data.pop('rate')
            coloursNPics = data.pop('coloursNPics', None)
            discounts = data.pop('discounts', None)
            for field in data:
                setattr(item, field, data[field])
            col_name = 'rate'+ str(rate)
            field_val = getattr(item, col_name)
            setattr(item, col_name, field_val+1)
            item.save()
            if coloursNPics:
                add_colsNpics(coloursNPics, recordCol, item)
            if discounts:
                add_discounts(discounts, recordDisc, item)
            return JsonResponse({"message": "Item updated", "item": model_to_dict(item)})
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def update_item_discount(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            record = models.Discount.objects.filter(discountID=data['discountID']).first()
            del data['discountID']
            bonusID = data.pop('bonusItemID', None)
            for field in data:
                setattr(record, field, data[field])
            if bonusID:
                extra = models.Item.objects.filter(itemID=bonusID).first()
                setattr(record, 'bonusItemID', extra)
            record.save()
            return JsonResponse({"message": "Updated successfully", "record": model_to_dict(record)})
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def update_coloursnpics(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            record = models.ColoursNPics.objects.filter(coloursNPicsID=data['coloursNPicsID']).first()
            del data['coloursNPicsID']
            for field in data:
                setattr(record, field, data[field])
            record.save()
            return JsonResponse({"message": "Updated successfully", "record": model_to_dict(record)})
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def search_items(request):
    if request.method == 'POST':
        try:
            query_list = json.loads(request.body)['query']
            all_ids = []
            #matching by name
            for query in query_list:
                theNames = models.NameAndCategory.objects.filter(name__contains=query).values('name')
                theNames = list(theNames)
                for el in range(len(theNames)):
                    theNames[el] = theNames[el]['name']
                for theName in theNames:
                    the_ids = models.Item.objects.filter(name=theName, itemDeleted=False).values('itemID').order_by("-rate5", "-rate4","-rate3","-rate2","-rate1")
                    for a_id in the_ids:
                        all_ids.append(a_id['itemID'])
            #now by description
            for query in query_list:
                the_ids = models.Item.objects.filter(description__contains=query, itemDeleted=False).order_by("-rate5", "-rate4","-rate3","-rate2","-rate1").values("itemID")
                for item in the_ids:
                    all_ids.append(item["itemID"])
            data = item_data(list(set(all_ids)), 'id')
            return JsonResponse(data)
            #would have added matching category, matching price, but the juice is not worth the squeeze
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def make_order(request):
    if request.method == 'POST':
        order_data = json.loads(request.body)
        try:
            new_world_order = None
            new_order = None
            orderer = models.UserInfo.objects.filter(userID=order_data['userID']).first()
            new_world_order = models.CustomerOrder.objects.create(userID=orderer)
            for instance in order_data['subOrders']:
                the_item = models.Item.objects.filter(itemID=instance['itemID'], itemDeleted=False).first()
                qty = instance['quantity'] if 'quantity' in instance else 1
                if the_item.stock < int(qty):
                    raise Exception("Item: " + the_item.name.name + " has insufficient stock.")
                coloursNPicsRow = models.ColoursNPics.objects.filter(coloursNPicsID=instance['coloursNPicsID']).first()
                new_order = models.OrderDetail.objects.create(orderID=new_world_order, itemName=the_item.name, coloursNPicsID=coloursNPicsRow, businessID=the_item.businessID)
                del instance['itemID']
                del instance['coloursNPicsID']
                for field in instance:
                    setattr(new_order, field, instance[field])
                setattr(the_item, "stock", int(the_item.stock)- int(qty))
                the_item.save()
                new_order.save()
            del order_data['subOrders']
            del order_data['userID']
            for field in order_data:
                setattr(new_world_order, field, order_data[field])
            new_world_order.save()
            return JsonResponse({"message": "order made", "orderID": new_world_order.orderID})
        except Exception as e:
            if new_world_order:
                new_world_order.delete()
            if new_order:
                new_order.delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def update_order(request):
    if request.method == 'POST':
        order_data = json.loads(request.body)
        try:
            order = models.CustomerOrder.objects.filter(orderID=order_data['orderID']).first()
            for ordd in order_data['subOrders']:
                item = models.Item.objects.filter(itemID=ordd['itemID'], itemDeleted=False).first()
                itemName = models.NameAndCategory.objects.filter(name=item.name).first()
                detail = models.OrderDetail.objects.filter(orderID=order_data['orderID'], itemName=itemName).first()
                del ordd['itemID']
                for field in ordd:
                    setattr(detail, field, ordd[field])
                detail.save()
            del order_data['subOrders']
            del order_data['orderID']
            for field in order_data:
                setattr(order, field, order_data[field])
            order.save()
            return JsonResponse({"message": "Order updated", "orderID": order.orderID})
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def get_user_orders(request):
    if request.method == 'GET':
        try:
            userID = request.GET.get("userID")
            transactions = models.CustomerOrder.objects.filter(userID=userID)
            transactions_dict = {}
            for tran in transactions:                
                transactions_dict[tran.orderID] = order_details(tran)
            return JsonResponse(transactions_dict)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def order_details(orderModel):
    arrObj = {
        "subOrders": []
    }
    detailses = models.OrderDetail.objects.filter(orderID=orderModel.orderID)
    for details in detailses:
        details = model_to_dict(details)
        business = models.BusinessInfo.objects.filter(businessID=details['businessID']).first()
        business = model_to_dict(business)
        coloursNPics = models.ColoursNPics.objects.filter(coloursNPicsID=details['coloursNPicsID']).values('photo', 'colour').first()
        del details['coloursNPicsID']
        del details['businessID']
        del details['orderID']
        details['coloursNPics'] = coloursNPics
        details['business'] = business
        arrObj["subOrders"].append(details)
    orderModel = model_to_dict(orderModel)
    del orderModel['userID']
    arrObj.update(orderModel)
    return arrObj

def orderID_data(request):
    if request.method == 'GET':
        try:
            orderID = request.GET.get('orderID')
            transaction = models.CustomerOrder.objects.filter(orderID=orderID)
            details = order_details(transaction[0]) #must be a model, inside a queryset
            return JsonResponse(details)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def get_business_orders(request):
    if request.method == 'GET':
        try:
            businessID = request.GET.get("businessID")
            orders = models.OrderDetail.objects.filter(businessID=businessID)
            orders_dict = {'orders': []}
            for ordd in orders:
                coloursNPics = ordd.coloursNPicsID
                theID = coloursNPics.itemID.itemID
                coloursNPics = model_to_dict(coloursNPics)
                coloursNPics['itemID'] = theID
                ordd = model_to_dict(ordd)
                del ordd['coloursNPicsID']
                ordd.update(coloursNPics)
                orders_dict['orders'].append(ordd)
            return JsonResponse(orders_dict)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def new_user(request):
    if request.method == 'POST':
        try:
            user = None #in case exception happens early
            userdata = json.loads(request.body)
            verif_code = str(int(random()*1000000))
            if "email" in userdata and not re.fullmatch(r"^[^\s@]+@[^\s@]+\.[^\s@]+$", userdata['email']):
                raise Exception("Not valid email")
            user = models.UserInfo.objects.create(firstname=userdata["firstname"], lastname=userdata["lastname"],password=userdata["password"], verificationCode=verif_code)
            for field in ["email", "phone", "latitude", "longitude"]:
                if field in userdata:
                    setattr(user, field, userdata[field])
            user.save()
            if "email" in userdata:
                mailsent = send_verif_email(userdata['email'], verif_code)
                if not mailsent:
                    raise Exception('Mail not sent')
            return JsonResponse({"message": "Unverified account created", "userID": user.userID})
        except Exception as e:
            if user:
                user.delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def verify_account(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            user = None
            if 'userID' in data:
                user = models.UserInfo.objects.filter(userID=data['userID']).first()
            elif 'businessID' in data:
                user = models.BusinessInfo.objects.filter(businessID=data['businessID']).first()
            if data["verificationCode"] == user.verificationCode:
                user.verificationCode = None
                user.save()
                return JsonResponse({"message": "Account Verified"})
            else:
                raise Exception("Verification failed")
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()}) 
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

def verify_email_update(request):
    try:
        data = json.loads(request.body)
        mail_record = None
        user_account = None
        if 'userID' in data:
            mail_record = models.EmailUpdate.objects.filter(userID=data['userID']).first()
            user_account = models.UserInfo.objects.filter(userID=data['userID']).first()
            if mail_record.verificationCode != data['verificationCode']:
                raise Exception("Verification failed")
            else:
                user_account.email = mail_record.email
                mail_record.delete()
                user_account.save()
                return JsonResponse({"message": "Email updated"})
        elif 'businessID' in data:
            user = models.EmailUpdate.objects.filter(businessID=data['businessID']).first()
            user_account = models.BusinessInfo.objects.filter(businessID=data['businessID']).first()
            if mail_record.verificationCode != data['verificationCode']:
                raise Exception("Verification failed")
            else:
                user_account.email = mail_record.email
                mail_record.delete()
                user_account.save()
                return JsonResponse({"message": "Email updated"})
    except Exception as e:
        return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})

def new_business(request):
    if request.method == 'POST':
        try:
            business = None #in case exception happens early
            data = json.loads(request.body)
            verif_code = str(int(random()*1000000))
            if "businessEmail" in data and not re.fullmatch(r"^[^\s@]+@[^\s@]+\.[^\s@]+$", data['businessEmail']):
                raise Exception("Not valid email")
            owner = models.UserInfo.objects.filter(userID=data['userID'], verificationCode__isnull=True).first()
            business = models.BusinessInfo.objects.create(userID=owner, verificationCode=verif_code, businessName=data['businessName'])
            for field in ["businessPhone", "businessEmail", "latitude", "longitude", "profilePhoto", "supportsCash", "supportsPaymentCard", "supportsMobileMoney"]:
                if field in data:
                    setattr(business, field, data[field])
            business.save()
            if "businessEmail" in data:
                mailsent = send_verif_email(data['businessEmail'], verif_code)
                if not mailsent:
                    raise Exception('Mail not sent')
            return JsonResponse({"message": "Unverified merchant account created", "businessID": business.businessID})
        except Exception as e:
            if business:
                business.delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})    
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def get_user_businesses(request):
    if request.method == 'GET':
        try:
            userID = request.GET.get('userID')
            businesses = models.BusinessInfo.objects.filter(userID=userID).values('businessName', 'businessID', 'businessPhone', 'businessEmail', 'latitude', 'longitude', 'profilePhoto')
            collection = {'businesses': list(businesses)}
            return JsonResponse(collection)
        except Exception as e:
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})    
    else:
        return JsonResponse({'error': 'Only GET requests are allowed.'})

def update_businessinfo(request):
    if request.method == 'POST':
        try:
            temp_user = None #for in case exception happens early
            updates = json.loads(request.body)
            record = models.BusinessInfo.objects.filter(businessID=updates["businessID"]).filter(verificationCode__isnull=True).first()
            del updates["businessID"]
            if "businessEmail" in updates and not re.fullmatch(r"^[^\s@]+@[^\s@]+\.[^\s@]+$", updates['businessEmail']):
                raise Exception("Not valid email")
            if not record.accountActive:
                raise Exception("Business account was deleted")
            for key in updates:
                if key not in ["businessID", "userID", "businessEmail", "registrationNumber", "profilePhoto"]: #last two in case frontend sends them
                    setattr(record, key, updates[key])
                elif key == "businessEmail":
                    verif_code = str(int(random()*1000000))
                    temp_user = models.EmailUpdate.objects.create(verificationCode=verif_code, businessID=record, email=updates["businessEmail"])
                    mailSent = send_verif_email(updates['businessEmail'], verif_code)
                    if not mailSent:
                        raise Exception('Mail not sent')
                    temp_user.save()
            
            record.save()
            record = model_to_dict(record)
            del record['verificationCode']
            del record['accountActive']
            del record['businessEmail'] #still reflecting the old one sometimes
            return JsonResponse(record)
        except Exception as e:
            if temp_user:
                temp_user.delete()
            return JsonResponse({'message': 'Invalid data provided.', 'err': e.__str__()})
    else:
        return JsonResponse({'error': 'Only POST requests are allowed.'})

