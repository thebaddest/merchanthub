# Generated by Django 4.2.9 on 2024-03-21 21:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('name', models.CharField(max_length=50)),
                ('category', models.CharField(choices=[('Eletronics', 'Electronics'), ('Gadgets', 'Gadgets'), ('Clothes', 'Clothes'), ('Shoes', 'Shoes'), ('Kitchenware', 'Kitchenware'), ('Services', 'Services'), ('Scholastic', 'Scholastic'), ('Books', 'Books'), ('Beddings', 'Beddings'), ('Hardware', 'Hardware'), ('Art and craft', 'Art and craft'), ('Food', 'Food'), ('Beverages', 'Beverages'), ('Furniture', 'Furniture'), ('Firearms', 'Firearms')], max_length=20, null=True)),
                ('itemID', models.AutoField(primary_key=True, serialize=False)),
                ('price', models.FloatField(default=0)),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('description', models.CharField(blank=True, max_length=500, null=True)),
                ('photo1', models.ImageField(blank=True, null=True, upload_to='')),
                ('photo2', models.ImageField(blank=True, null=True, upload_to='')),
                ('photo3', models.ImageField(blank=True, null=True, upload_to='')),
                ('photo4', models.ImageField(blank=True, null=True, upload_to='')),
                ('rate1', models.IntegerField(default=0)),
                ('rate2', models.IntegerField(default=0)),
                ('rate3', models.IntegerField(default=0)),
                ('rate4', models.IntegerField(default=0)),
                ('rate5', models.IntegerField(default=0)),
                ('colour1', models.CharField(blank=True, max_length=15, null=True)),
                ('colour2', models.CharField(blank=True, max_length=15, null=True)),
                ('colour3', models.CharField(blank=True, max_length=15, null=True)),
                ('colour4', models.CharField(blank=True, max_length=15, null=True)),
                ('colour5', models.CharField(blank=True, max_length=15, null=True)),
                ('itemStatus', models.CharField(choices=[('Deleted', 'Deleted'), ('Present', 'Present')], default='Present', max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('userID', models.AutoField(primary_key=True, serialize=False)),
                ('firstname', models.CharField(max_length=30)),
                ('lastname', models.CharField(max_length=30)),
                ('email', models.CharField(blank=True, max_length=30, null=True, unique=True)),
                ('password', models.CharField(max_length=30)),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('phone', models.CharField(blank=True, max_length=10, null=True, unique=True)),
                ('accountStatus', models.CharField(choices=[('Deleted', 'Deleted'), ('Active', 'Active')], default='Active', max_length=7)),
            ],
        ),
        migrations.CreateModel(
            name='UserInfo_unverified',
            fields=[
                ('userID', models.AutoField(primary_key=True, serialize=False)),
                ('firstname', models.CharField(max_length=30)),
                ('lastname', models.CharField(max_length=30)),
                ('email', models.CharField(blank=True, max_length=30, null=True, unique=True)),
                ('password', models.CharField(max_length=30)),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('phone', models.CharField(blank=True, max_length=10, null=True, unique=True)),
                ('verification_code', models.CharField(max_length=6)),
            ],
        ),
        migrations.CreateModel(
            name='TransactionHistory',
            fields=[
                ('orderID', models.AutoField(primary_key=True, serialize=False)),
                ('itemName', models.CharField(max_length=30, null=True)),
                ('itemPhoto', models.ImageField(blank=True, null=True, upload_to='')),
                ('itemColour', models.CharField(blank=True, max_length=15, null=True)),
                ('totalBill', models.FloatField(default=0)),
                ('quantity', models.IntegerField(default=1)),
                ('latitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('longitude', models.DecimalField(blank=True, decimal_places=6, max_digits=9, null=True)),
                ('paymentStatus', models.BooleanField(default=False)),
                ('deliveryStatus', models.BooleanField(default=False)),
                ('cancellationStatus', models.BooleanField(default=False)),
                ('transactionDate', models.DateTimeField(auto_now_add=True)),
                ('serviceStart', models.DateTimeField(blank=True, null=True)),
                ('serviceEnd', models.DateTimeField(blank=True, null=True)),
                ('userID', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='backend.userinfo')),
            ],
        ),
        migrations.CreateModel(
            name='Discount',
            fields=[
                ('discountID', models.AutoField(primary_key=True, serialize=False)),
                ('minQty', models.IntegerField(default=1)),
                ('cashDiscounted', models.FloatField(default=0)),
                ('offerExpiry', models.DateTimeField(blank=True, null=True)),
                ('bonusItemID', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='discounts_bonusItem', to='backend.item')),
                ('itemID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='discounts_itemID', to='backend.item')),
            ],
        ),
        migrations.CreateModel(
            name='Card',
            fields=[
                ('name', models.CharField(max_length=60)),
                ('cardnum', models.CharField(max_length=30, primary_key=True, serialize=False)),
                ('expdate', models.DateField()),
                ('securityCode', models.CharField(max_length=3)),
                ('userID', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='backend.userinfo')),
            ],
        ),
    ]
