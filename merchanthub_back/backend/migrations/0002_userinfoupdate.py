# Generated by Django 4.2.11 on 2024-03-22 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserInfoUpdate',
            fields=[
                ('userID', models.IntegerField(primary_key=True, serialize=False)),
                ('email', models.CharField(blank=True, max_length=30, null=True)),
                ('phone', models.CharField(blank=True, max_length=10, null=True)),
                ('verification_code', models.CharField(max_length=6)),
            ],
        ),
    ]
