# Generated by Django 4.2.9 on 2024-04-11 20:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0007_businessinfo_accountactive'),
    ]

    operations = [
        migrations.DeleteModel(
            name='AvailableTime',
        ),
    ]
