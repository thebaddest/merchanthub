from django.urls import path
from . import views

urlpatterns = [
    path('getcards', views.get_cards),
    path('createcard/',views.create_card),
    path('removecard/',views.remove_card),
    path('getcatalogue', views.get_catalogue),
    path('itemsbycat', views.items_by_cat),
    path('itemsbyname/', views.items_by_name),
    path('itemsbyitemID/', views.items_by_itemID),
    path('itemsbybusinessID', views.items_by_businessID),
    path('additem/', views.add_item),
    path('updateitem/', views.update_item),
    path('updatecoloursnpics/', views.update_coloursnpics),
    path('updateitemdiscount/', views.update_item_discount),
    path('searchitems/', views.search_items),
    path('makeorder/', views.make_order),
    path('updateorder/',views.update_order),
    path('getuserorders', views.get_user_orders),
    path('getbusinessorders', views.get_business_orders),
    path('orderIDdata', views.orderID_data),
    path('verifyaccount/', views.verify_account),
    path('updateuserinfo/',views.update_userinfo),
    path('loginuser/', views.login_user),
    path('newuser/', views.new_user),
    path('verifyemailupdate/', views.verify_email_update),
    path('getuserbusinesses', views.get_user_businesses),
    path('newbusiness/', views.new_business),
    path('updatebusinessinfo/', views.update_businessinfo),
    path('', views.initialise)
]

