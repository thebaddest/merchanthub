import unittest
from django.test import TestCase
from backend.models import OrderDetail,CustomerOrder,ColoursNPics,Card,Discount,Item,NameAndCategory,EmailUpdate,BusinessInfo,UserInfo

class Testcase_for_models(TestCase):
    def setUp(self):
        # Create test data for UserInfo
        self.user1 = UserInfo.objects.create(firstname="claire", lastname="leah", email="claire@gmail.com", password="password123", phone="1234567890", verificationCode="123456")
        
        # Create test data for BusinessInfo
        self.business1 = BusinessInfo.objects.create(businessName="Test Business", userID=self.user1, businessPhone="0987654321", businessEmail="business@example.com",verificationCode="654321")
        
        # Create test data for NameAndCategory
        self.category1 = NameAndCategory.objects.create(category="Test Category", name="Test Item")
        
        # Create test data for Item
        self.item1 = Item.objects.create(name=self.category1, businessID=self.business1, stock=10, price=20.0, description="Test item description")
        
        # Create test data for Discount
        self.discount1 = Discount.objects.create(itemID=self.item1, minQty=5, cashDiscounted=5.0)
        
        # Create test data for Card
        self.card1 = Card.objects.create(name="claire leah", cardNum="1234567890123456", securityCode="123", userID=self.user1)
        
        # Create test data for ColoursNPics
        self.colour1 = ColoursNPics.objects.create(itemID=self.item1, photo="test.jpg", colour="Red")
        
        # Create test data for CustomerOrder
        self.order1 = CustomerOrder.objects.create(userID=self.user1, totalBill=100.0)
        
        # Create test data for OrderDetail
        self.orderDetail1 = OrderDetail.objects.create(orderID=self.order1, businessID=self.business1, itemName=self.category1, quantity=2)

    def test_user_info(self):
        user = UserInfo.objects.get(email="claire@gmail.com")
        self.assertEqual(user.firstname, "claire")
        self.assertEqual(user.lastname, "leah")
        self.assertEqual(user.phone, "1234567890")
    
    def test_business_info(self):
        business = BusinessInfo.objects.get(businessName="Test Business")
        self.assertEqual(business.userID, self.user1)
        self.assertEqual(business.businessPhone, "0987654321")

    def test_name_and_category(self):
        category = NameAndCategory.objects.get(category="Test Category")
        self.assertEqual(category.name, "Test Item")

    def test_item(self):
        item = Item.objects.get(description="Test item description")
        self.assertEqual(item.stock, 10)
    
    def test_discount(self):
        discount = Discount.objects.get(minQty=5)
        self.assertEqual(discount.cashDiscounted, 5.0)
    
    def test_card(self):
        card = Card.objects.get(cardNum="1234567890123456")
        self.assertEqual(card.name, "claire leah")
    
    def test_colours_n_pics(self):
        colour = ColoursNPics.objects.get(colour="Red")
        self.assertEqual(colour.photo, "test.jpg")
    
    def test_customer_order(self):
        order = CustomerOrder.objects.get(totalBill=100.0)
        self.assertEqual(order.userID, self.user1)
    
    def test_order_detail(self):
        order_detail = OrderDetail.objects.get(quantity=2)
        self.assertEqual(order_detail.orderID, self.order1)
 