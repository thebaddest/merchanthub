from django.db import models

class UserInfo(models.Model): #information personally identifying the buyer
    userID = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length = 30)
    lastname = models.CharField(max_length = 30)
    email = models.CharField(max_length = 30, null=True, unique=True, blank=True)
    password = models.CharField(max_length = 30)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    phone = models.CharField(max_length = 10, null=True, unique=True, blank=True)
    accountActive = models.BooleanField(default=True)
    verificationCode =models.CharField(max_length =6, null=True)

    def __str__(self):
        return str(self.userID)

class BusinessInfo(models.Model):
    businessName = models.CharField(max_length=30)
    businessID = models.AutoField(primary_key=True)
    userID = models.ForeignKey(UserInfo,on_delete=models.CASCADE)
    businessPhone = models.CharField(max_length = 10, null=True, blank=True)
    businessEmail = models.CharField(max_length = 30, null=True, blank=True)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    supportsCash = models.BooleanField(default=True)
    supportsMobileMoney = models.BooleanField(default=True)
    supportsPaymentCard = models.BooleanField(default=True)
    verificationCode = models.CharField(max_length=6, null=True)
    accountActive = models.BooleanField(default=True)

    def __str__(self):
        return self.businessName

class EmailUpdate(models.Model):
    updateID = models.AutoField(primary_key=True)
    userID = models.ForeignKey(UserInfo, on_delete=models.CASCADE, null=True, blank=True)
    businessID = models.ForeignKey(BusinessInfo, on_delete=models.CASCADE, null=True, blank=True)
    email = models.CharField(max_length = 30, null=True, blank=True)
    verificationCode = models.CharField(max_length =6)

    def __str__(self):
        return str(self.userID)

class NameAndCategory(models.Model):
    category = models.CharField(max_length=30)
    name = models.CharField(max_length=30, primary_key=True)

    def __str__(self):
        return self.name       

class Item(models.Model): #whether product or service
    name = models.ForeignKey(NameAndCategory, on_delete=models.CASCADE)
    itemID = models.AutoField(primary_key=True)
    businessID = models.ForeignKey(BusinessInfo,on_delete=models.SET_NULL,null=True)
    scheduleAppt = models.BooleanField(default=False)
    stock = models.IntegerField(default=1)
    price = models.FloatField(default=0)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    rate1 = models.IntegerField(default=0)
    rate2 = models.IntegerField(default=0)
    rate3 = models.IntegerField(default=0)
    rate4 = models.IntegerField(default=0)
    rate5 = models.IntegerField(default=0)
    itemDeleted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)

class Discount(models.Model):
    itemID = models.ForeignKey(Item, on_delete=models.CASCADE, related_name='discounts_itemID')
    discountID = models.AutoField(primary_key=True)
    minQty = models.IntegerField(default=1)
    cashDiscounted = models.FloatField(default=0)
    bonusItemID = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True, related_name='discounts_bonusItem', blank=True)
    offerExpiry = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.discountID)
    
class Card(models.Model): #payment card of buyer
    name = models.CharField(max_length = 60)
    cardNum = models.CharField(max_length = 30, primary_key = True)
    expDate = models.DateField(auto_now_add=True)
    userID = models.ForeignKey(UserInfo, on_delete=models.SET_NULL,null= True, blank=True)
    securityCode = models.CharField(max_length=3)
    businessID = models.ForeignKey(BusinessInfo,on_delete=models.SET_NULL,null=True, blank=True)

    def __str__(self):
        return self.name

class ColoursNPics(models.Model):
    coloursNPicsID = models.AutoField(primary_key=True)
    itemID = models.ForeignKey(Item,on_delete=models.CASCADE)
    photo = models.TextField(null=True, blank=True)
    colour = models.CharField(max_length=10,null= True,blank=True)

    def __str__(self):
        return str(self.coloursNPicsID)

class CustomerOrder(models.Model):
    orderID = models.AutoField(primary_key=True)
    userID = models.ForeignKey(UserInfo, on_delete=models.DO_NOTHING)
    totalBill= models.FloatField(default=0)
    paymentStatus = models.BooleanField(default=False)
    deliveryStatus = models.BooleanField(default=False)
    cancellationStatus = models.BooleanField(default=False)
    transactionDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.orderID)

class OrderDetail(models.Model):
    detailID = models.AutoField(primary_key=True)
    orderID = models.ForeignKey(CustomerOrder, on_delete=models.CASCADE)
    businessID = models.ForeignKey(BusinessInfo,on_delete=models.DO_NOTHING,null=True)
    itemName = models.ForeignKey(NameAndCategory, on_delete=models.DO_NOTHING, null=True)
    coloursNPicsID = models.ForeignKey(ColoursNPics, on_delete=models.DO_NOTHING, null=True)
    quantity = models.IntegerField(default=1)
    latitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    longitude = models.DecimalField(max_digits=9, decimal_places=6, null=True, blank=True)
    serviceStart = models.DateTimeField(null=True, blank=True)
    serviceEnd = models.DateTimeField(null=True, blank=True)

    def __str__(self):
      return str(self.detailID)


